set :application, "isi_mumbai"
set :repository,  "git@github.com:idyllicsoftware/isi_mumbai_go.git"
set :branch, "master"
set :deploy_to, "/home/sid/apps/#{application}/"
$:.unshift(File.expand_path('./lib', ENV['rvm_path']))

set :scm, :git 
# You can set :scm explicitly or Capistrano will make an intelligent guess based on known version control directory names
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

role :web, "ec2-54-235-216-228.compute-1.amazonaws.com"                          # Your HTTP server, Apache/etc
role :app, "ec2-54-235-216-228.compute-1.amazonaws.com"                          # This may be the same as your `Web` server
role :db,  "ec2-54-235-216-228.compute-1.amazonaws.com", :primary => true # This is where Rails migrations will run
role :db,  "ec2-54-235-216-228.compute-1.amazonaws.com"

set :keep_releases, 5   
set :use_sudo, false
set :user, "sid" 
#set :rvm_type, :user      
set :rvm_ruby_string, "ruby-1.9.2-p320"
set :deploy_via, :remote_cache 

# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
# namespace :deploy do
#   task :start do ; end
#   task :stop do ; end
#   task :restart, :roles => :app, :except => { :no_release => true } do
#     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
#   end
# end