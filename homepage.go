package main

import (
	"fmt"
	"net/http"
	"io"
	"io/ioutil"
	"log"
	"time"
)

type HandleFunc func(http.ResponseWriter, *http.Request)

//var templates = make(map[string]*template.Template)

func LogRequest(request *http.Request){
	log.Println(request.URL.Path)
}

func LogRedirect(request *http.Request, newUrl string, status int64){
	log.Printf("Redirected %v to %v [%v] \n", request.URL.Path, newUrl, status)
}

func logPanic(recieverFunction HandleFunc) HandleFunc{
	return func(writer http.ResponseWriter, request *http.Request){
		defer func(){
			if x := recover(); x != nil{
				log.Printf("Panic: [%v]: %v \n", request.RemoteAddr, x)
				go LogRedirect(request, "/", 302)
				http.Redirect(writer, request, "/", 302)
			}
		}()
		recieverFunction(writer, request)
	}
}

func HandlePanic(w http.ResponseWriter, r *http.Request){
	go LogRequest(r)
	panic("this ship is going down motherfucker")
}

func HandleHome(writer http.ResponseWriter, request *http.Request){
	go LogRequest(request)	
	writer.Header().Set("Content-Type", "text/html")
	fileText, err := ioutil.ReadFile("main.html")
	if err != nil {
		panic("Error in file reading, main.html")
	}	
	io.WriteString(writer, string(fileText))
	 
}

func init(){

}	

func main(){
	fmt.Printf("Request logged at %v", time.Now())	
	http.HandleFunc("/", logPanic(HandleHome))
	http.Handle("/img/",http.StripPrefix("/img", http.FileServer(http.Dir("public/img"))))
	http.Handle("/js/", http.StripPrefix("/js", http.FileServer(http.Dir("public/js"))))
	http.Handle("/css/", http.StripPrefix("/css", http.FileServer(http.Dir("public/css"))))
	http.HandleFunc("/panic", logPanic(HandlePanic))

	if err := http.ListenAndServe(":8799", nil); err != nil{
		panic(err)
	}
}